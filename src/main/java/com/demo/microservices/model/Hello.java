package com.demo.microservices.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Hello {
	
	private Long counter;
	private String msg;	
	
	

}
